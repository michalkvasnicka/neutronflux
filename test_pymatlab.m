% pyenv(Version="/usr/bin/python3.10");

clear classes
m = py.importlib.import_module('pynslr1d_extra');
py.importlib.reload(m);


% data = load("u1c35_210306.mat");
data = load("u2c35_220922.mat");


neutronflux = data.data.SPND(:,13)';
ts = (0:(length(neutronflux)-1));

forced_splits = data.data.control;

% add some additional forced_splits
forced_splits = sort([forced_splits;(1400:20:1500)']);
%forced_splits = sort([100, 101]);


forced_splits = forced_splits - 1;
% forced_splits = []; % no control

% choose different sigma_std
noise_std = 0.0005;
% noise_std = 1e-1;  % forced_splits segments are still the same
% noise_std = 1e-4;  % forced_splits segments are still the same 
% noise_std = 1e-6;  % NaN outputs

split_rate = 1/100.0;
max_hypotheses = 1000.0;

tic;
result = cell(py.pynslr1d_extra.nslr_forced_splits(ts, neutronflux, forced_splits, noise_std, split_rate, max_hypotheses));
toc

tss = double(result{1,1});
xss = double(result{1,2});
%[tss, xss, splits] = nslr_base(ts, neutronflux, param);

hold off;
plot(ts, neutronflux);
hold on;
plot(tss, xss,'ro-','LineWidth',3);
xline(forced_splits,'k--')