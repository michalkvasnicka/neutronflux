import scipy.io
import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint
import nslr
import scipy.stats
from scipy.interpolate import interp1d
import faster_nslr as slow_nslr

data = scipy.io.loadmat('TestData.mat', simplify_cells=True)

data = data["EDU_U1C33_181001"]

dt = 1.0
temperature = np.array(data['TC'])
neutron_current = np.array(data['SPND'])

x = temperature.T[1]
x = x[::-1]
x = x[:1000]
ts = np.arange(len(x))*1.0


noise_std = 0.00015
noise_std = 0.2
split_rate = 1/60.0

valid = np.isfinite(x)

seg = nslr.nslr1d(ts[valid], x[valid], noise=noise_std, split_rate=split_rate)

# Run the slow NSLR
segments_slow = slow_nslr.nslr(ts, x,
                noise_std,
                split_rate,
                )
segments_slow.x = np.array(segments_slow.x).reshape(-1)

plt.plot(ts, x, 'k.')
plt.plot(seg.t, seg.x, 'b', label='Old')
plt.plot(segments_slow.t, segments_slow.x, 'r', lw=3, label='New')

#print(segments_slow.outliers)
#plt.plot(ts[segments_slow.outliers], x[segments_slow.outliers], 'o')

plt.legend()
plt.show()
