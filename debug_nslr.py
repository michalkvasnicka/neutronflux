import cppimport.import_hook
import pynslr1d
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal

# Define noise and split rate of the signal
noise_std = 0.2
split_rate = 1/3.0
np.random.seed(0)

# Create a test signal
dt = 1/60.0
ts = np.arange(0, 1.0, dt)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth(ts/split_rate, width=0.3) + 10
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std

# Add outliers

outlier_i = np.flatnonzero(np.random.rand(len(xs)) < 0.1)
xs[outlier_i] += np.random.randn(len(outlier_i))*noise_std*5

nslr = pynslr1d.Nslr()
print(nslr.hypotheses[0].lik)
for x in xs[:3]:
    nslr.step(dt, x, noise_std, split_rate)
    #nslr.step(dt, 0.0, 1.0, 1/3.0)
    h = nslr.hypotheses[0]
    print(h.total_lik)
    #for h in nslr.hypotheses:
    #    print(h.lik, h.total_lik, h.b, h.a)
    #for h in nslr.hypotheses:
    #    print(h.j, h.lik)
    #    #print(h.j, h.parent)
    #print([h.j for h in nslr.hypotheses])
    print("---")
print("Here")
print(nslr.splits())


