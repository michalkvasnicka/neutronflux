from pynslr1d import *
import numpy as np
from scipy.interpolate import interp1d

def forced_split_segments(ts, xs, forced_splits, noise_std, split_rate, max_hypotheses=0):
    splits = []
    #splits = pynslr1d.nslr_segments(ts[valid], x[valid], noise_std, 1/300.0, max_hypotheses=100)
    subidx = [0] + list(forced_splits) + [len(ts)]
    for i in range(len(subidx) - 1):
        s, e = int(subidx[i]), int(subidx[i+1])
        # TODO: In this implementation the endpoints of the forced segments are fed twice.
        # This is hard to remedy with the current implementation, but could be handled if
        # the forced splitting was within the algorithm itself.
        subsplits = nslr_segments(ts[s:e+1], xs[s:e+1], noise_std, split_rate, int(max_hypotheses))
        splits += [s+j for j in subsplits[:-1]]
    
    splits.append(len(ts) - 1)
    return splits

def nslr_forced_splits(ts, xs, forced_splits, noise_std, split_rate, max_hypotheses=0):
    ts = np.array(ts)
    xs = np.array(xs)

    splits = forced_split_segments(ts, xs, forced_splits, noise_std, split_rate, max_hypotheses)
    ends = segmented_linear_fit(ts, xs, splits)
    return ts[splits], ends, splits

def nslr_forced_splits_auto_std(ts, xs, forced_splits, split_rate, max_hypotheses=0, n_iterations=10):
    ts = np.array(ts)
    xs = np.array(xs)
    noise_std = np.std(xs)
    
    for i in range(n_iterations):
        tss, xss, splits = nslr_forced_splits(ts, xs, forced_splits, noise_std, split_rate, max_hypotheses)
        residual = interp1d(tss, xss)(ts) - xs
        noise_std = np.std(residual)
    
    return ts[splits], xss, splits
