// cppimport
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "nslr1d.hpp"

namespace py = pybind11;
using namespace pybind11::literals;

PYBIND11_MODULE(pynslr1d, m) {
	m.def("nslr", nslr, "ts"_a, "xs"_a, "noise_std"_a, "split_rate"_a, "max_hypotheses"_a=0);
	m.def("nslr_segments", nslr_segments, "ts"_a, "xs"_a, "noise_std"_a, "split_rate"_a, "max_hypotheses"_a=0);
	m.def("segmented_linear_fit", segmented_linear_fit);
	m.def("segment_coefficients", segment_coefficients);
	
	py::class_<NslrHypothesis, NslrHypothesisPtr>(m, "NslrHypothesis")
		.def_readwrite("j", &NslrHypothesis::j)
		.def_readwrite("t", &NslrHypothesis::t)
		.def_readwrite("a", &NslrHypothesis::a)
		.def_readwrite("b", &NslrHypothesis::b)
		.def_readwrite("lik", &NslrHypothesis::lik)
		.def_readwrite("total_lik", &NslrHypothesis::total_lik)
		.def_readwrite("parent", &NslrHypothesis::parent)
	;

	py::class_<Nslr>(m, "Nslr")
		.def(py::init<>())
		.def("step", &Nslr::step, "dt"_a, "x"_a, "noise_std"_a, "split_rate"_a, "max_hypotheses"_a=0)
		.def("splits", &Nslr::splits)
		.def_readwrite("hypotheses", &Nslr::hypotheses)
	;
}

/*
<%
cfg['dependencies'] = ['nslr1d.hpp']
cfg['extra_compile_args'] = ['-O4']
%>
*/
