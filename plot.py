import scipy.io
import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint
import nslr
import scipy.stats
from scipy.interpolate import interp1d
import cppimport.import_hook
import pynslr1d

"""
Notes:

- Neutron current seems very good fit for NSLR.
- Very different noise levels at different neutron current channels?
- Why control rod changes reflected only at some assemblies?

- What counts as an outlier?
- Temperature discretization errors quite clear
- Temperature noise distribution indeed non-Gaussian
- Especially interested in the "steps"?

- Is there a correspondence between the temperature and neutron current signals?
  Is there knowledge of e.g. which fuel assembly which signals belong to?

- Good NSLR parameterization seem a bit weird, I have to check
- NSLR doesn't seem to do much 1-sample segments with neutron current

- WTF happens in channel 16!?! Channel 20, channel 29!?
- Channel 23, 24, 25, 26, 27 correlated noise/oscillation

- Automatic bad fit detection?
- Automatic noise estimation

- Parameterization bad for some channels (e.g. 31)

- Temperature signal very difficult
  - May need a totally different approach. E.g. nonlinear Bayesian smoothing?
"""

data = scipy.io.loadmat('TestData.mat', simplify_cells=True)

data = data["EDU_U1C33_181001"]

rod_change = data['control']

dt = 1.0
temperature = np.array(data['TC'])
neutron_current = np.array(data['SPND'])

ts = np.arange(len(temperature[:,0]))*1.0

#plt.plot(ts, data['Pa'])
#for idx in rod_change:
#    plt.axvline(ts[idx], color='black')
#plt.show()

"""
from sklearn.decomposition import PCA, FastICA

pca = PCA(whiten=True).fit(neutron_current)
wtf = pca.transform(neutron_current)
print(pca.components_)
print(wtf.shape, pca.components_.shape)
for c, w in zip(wtf.T, pca.components_):
    order = np.argsort(np.abs(w))[::-1]
    plt.plot(c)
    for idx in order[:3]:
        plt.plot(scipy.stats.zscore(neutron_current.T[idx]))
    plt.show()
"""

import networkx as nx
corr = np.corrcoef(neutron_current.T)

distance = np.sqrt(2*(1 - corr))

from sklearn.manifold import MDS, SpectralEmbedding, Isomap, TSNE, LocallyLinearEmbedding



#graph = nx.from_numpy_matrix(distance)
#wtf = nx.nx_pydot.pydot_layout(graph, prog='twopi')
#assembly_positions = np.array(list(wtf.values()))

assembly_positions = MDS(dissimilarity='precomputed').fit_transform(distance)

"""
normed_neutron_current = scipy.stats.zscore(neutron_current, axis=0)
for t in list(range(len(ts)))[::100]:
    print(t)
    plt.cla()
    plt.scatter(*assembly_positions.T, c=normed_neutron_current[t])
    plt.clim(-2, 2)
    plt.pause(0.001)

plt.plot(*wtf.T, 'o')
plt.show()
"""
#for w in corr:
#    plt.plot(w, 'o-')
#    plt.show()

"""
for j in range(len(corr)):
    #plt.plot(neutron_current.T[j])
    #plt.twinx()
    #closest = np.argsort(corr[j])[::-1]
    closest = np.flatnonzero(corr[j] > 0.8)
    print(len(closest))
    for c in closest[:10]:
        plt.plot(scipy.stats.zscore(neutron_current.T[c]), alpha=0.3)
    plt.show()
"""

#plt.matshow(corr)
#plt.colorbar()
#plt.show()
print(corr)

#noise_std = 0.00015 # This is the proper noise scale!!
# Use something like this for the temperature
#noise_std = 1.0
# TODO: Segfault when noise_std=100.0 is used for the neutron current!
#for i, x in enumerate(neutron_current.T):
for i, x in enumerate(temperature.T):
    plt.figure("timeseries")
    plt.title(f"Channel {i}")
    valid = np.isfinite(x)
    if np.sum(valid) < 2: continue
    plt.plot(ts, x, 'k-', alpha=0.1, label='Measurement')
    
    noise_std = np.std(x[valid])
    
    """
    seg = nslr.nslr1d(ts[valid], x[valid], noise=noise_std, split_rate=1/600.0)
    
    #interp = scipy.interpolate.interp1d(seg.t, np.array(seg.x).reshape(-1))
    #residual = interp(ts[valid]) - x[valid]
    #print(np.std(residual))
    #plt.figure("residual")
    #plt.hist(residual, bins='scott', density=True)
    
    plt.figure("timeseries")
    
    plt.plot(seg.t, seg.x, label='NSLR slow change', color='C1', lw=3)
    
    seg = nslr.nslr1d(ts[valid], x[valid], noise=noise_std, split_rate=1/60.0)
    plt.plot(seg.t, seg.x, label='NSLR fast change', color='C0', lw=3)
    """
    
    import faster_nslr
    for i in range(3):
        splits = pynslr1d.nslr_segments(ts[valid], x[valid], noise_std, 1/300.0)
        ends = faster_nslr.segmented_linear_fit(ts[valid], x[valid].reshape(-1, 1), splits)
        ends = np.array(ends).reshape(-1)
        # TODO: We shouldn't get out of bounds, but the ending handling isn't
        # entirely clear in the new C++ implementation
        fit = interp1d(ts[valid][splits], ends, bounds_error=False)
        residual = fit(ts[valid]) - x[valid]
        noise_std = np.nanstd(residual)

    
    plt.plot(ts[splits], ends, '-', color='orange', label="NSLR new", lw=2)



    for idx in rod_change:
        plt.axvline(ts[idx], color='black')

    plt.xlabel("Time (seconds)")
    plt.ylabel("Value")
    plt.legend()
    
    #plt.figure()
    #plt.hist(np.diff(ts[valid][splits]), bins=100)

    plt.show()

#plt.plot(neutron_current[:,0])
plt.show()
