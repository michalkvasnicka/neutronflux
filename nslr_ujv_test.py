import numpy as np
from scipy.interpolate import interp1d
import scipy.io
import matplotlib.pyplot as plt

import cppimport.import_hook
import pynslr1d
import faster_nslr
from pynslr1d_extra import forced_split_segments, nslr_forced_splits



#data = scipy.io.loadmat('TestData.mat', simplify_cells=True)
#data = data["EDU_U1C33_181001"]

data = scipy.io.loadmat('u1c35_210306.mat', simplify_cells=True)
#data = scipy.io.loadmat('u2c35_220922.mat', simplify_cells=True)
data = data['data']


rod_change = data['control']

dt = 1.0
temperature = np.array(data['TC'])
neutron_current = np.array(data['SPND'])


#signals = temperature.T
signals = neutron_current.T
ts = np.arange(len(signals[0]))*dt

"""
def forced_split_nslr_segments(ts, xs, forced_splits, noise_std, split_rate, max_hypotheses=0):
    splits = []
    #splits = pynslr1d.nslr_segments(ts[valid], x[valid], noise_std, 1/300.0, max_hypotheses=100)
    subidx = [0] + list(forced_splits) + [len(ts)]
    for i in range(len(subidx) - 1):
        s, e = subidx[i], subidx[i+1]
        subsplits = pynslr1d.nslr_segments(ts[s:e], x[s:e], noise_std, split_rate, max_hypotheses)
        splits += [s+j for j in subsplits[:-1]]
    
    return splits
"""

for i, x in enumerate(signals):
    plt.figure("timeseries")
    plt.title(f"Channel {i}")
    valid = np.isfinite(x)
    if np.sum(valid) < 2: continue
    plt.plot(ts, x, 'k-', alpha=0.5, label='Measurement')
    
    # Initialize the noise std with the total std. This will be
    # re-estimated below
    noise_std = np.std(x[valid])
    
    # In the original NSLR the re-estimations are run until the segmentation
    # doesn't change. The current Python implementation of segmented_linear_fit
    # is too slow to do this in practice.
    n_reestimations = 3
    import time
    
    forced_splits = rod_change

    for i in range(n_reestimations):
        perf_t = time.perf_counter()
        #splits = pynslr1d.nslr_segments(ts[valid], x[valid], noise_std, 1/300.0, max_hypotheses=100)
        #splits = forced_split_nslr_segments(ts[valid], x[valid], forced_splits, noise_std, 1/100.0, max_hypotheses=100)
        print("Split samples per second: ", int(np.sum(valid)/(time.perf_counter() - perf_t)))
        perf_t = time.perf_counter()
        n = np.sum(valid)
        #if splits[-1] == n-1:
        #    splits[-1] += 1
        #else:
        #    splits.append(n)
        #ends = pynslr1d.segmented_linear_fit(ts[valid], x[valid], splits)
        #splits[-1] -= 1
        tss, ends, splits = nslr_forced_splits(ts[valid], x[valid], forced_splits, noise_std, 1/100.0, max_hypotheses=100)
        print("Regression samples per second: ",  int(np.sum(valid)/(time.perf_counter() - perf_t)))
        #ends = np.array(ends).reshape(-1)
        # TODO: We shouldn't get out of bounds, but the ending handling isn't
        # entirely clear in the new C++ implementation
        
        # Re-estimate the noise level using the fit residual. This
        # could be a lot nicer, e.g. taking into account the number of samples in the
        # segments. However, for eye movements at least this seems to work quite well
        # in practice.
        fit = interp1d(ts[valid][splits], ends, bounds_error=False)
        residual = fit(ts[valid]) - x[valid]
        noise_std = np.nanstd(residual)

    plt.plot(ts[splits], ends, '.-', color='orange', label="NSLR new", lw=2)

    for idx in rod_change:
        plt.axvline(ts[idx], color='black', ymax=0.05)

    plt.xlabel("Time (seconds)")
    plt.ylabel("Value")
    plt.legend()
    
    plt.show()
