#include "mex.h"
#include "../nslr1d.hpp"

double getParam(const mxArray* p, const char* key) {
	mxArray* val = mxGetField(p, 0, key);
	if(!val) {
		mexErrMsgIdAndTxt("nslr:nofield", "No value for %s", key);
		return 0.0;
	}

	return mxGetScalar(val);
}

void mexFunction(int nlhs, mxArray *outputs[], int nrhs, const mxArray *inputs[])
{
	if(nrhs < 3) {
		mexErrMsgIdAndTxt("nslr:noargs", "At least 3 arguments required");
		return;
	}

    std::cout << "Running NSLR" << std::endl;
	
	const mxArray* ts_m = inputs[0];
	const mxArray* xs_m = inputs[1];
	const mxArray* p = inputs[2];
	size_t len = mxGetN(ts_m);
	Doubles ts(mxGetPr(ts_m), len);
	Doubles xs(mxGetPr(xs_m), len);

	std::vector<double> tss;
	std::vector<double> xss;
	Uints splits;

	std::tie(tss, xss, splits) = nslr(ts, xs,
			getParam(p, "noise_std"),
			getParam(p, "split_rate"),
			getParam(p, "max_hypotheses")
			);
	size_t slen = tss.size();
	std::cout << "N segments " << slen << std::endl;
    mxArray *tss_out = mxCreateDoubleMatrix(1, slen, mxREAL);
    mxArray *xss_out = mxCreateDoubleMatrix(1, slen, mxREAL);
    mxArray *splits_out = mxCreateNumericMatrix(1, slen, mxUINT64_CLASS, mxREAL);

	
	// TODO: Have to copy as the NSLR API doesn't allow for passing
	// the output matrix. Will be tricky anyway, as the number of segments
	// isn't know ahead of time
	double* tss_ptr = mxGetPr(tss_out);
	double* xss_ptr = mxGetPr(xss_out);
    UINT64_T* splits_ptr = (UINT64_T*)mxGetData(splits_out);
    for(size_t i=0; i < slen; ++i) {
		tss_ptr[i] = tss[i];
        xss_ptr[i] = xss[i];
        splits_ptr[i] = splits[i];
	}

	outputs[0] = tss_out;
    outputs[1] = xss_out;
    outputs[2] = splits_out;
}
