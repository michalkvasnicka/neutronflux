data = load("../u1c35_210306.mat");

neutronflux = data.data.SPND(:,1)';
ts = (0:(length(neutronflux)-1));


param = struct(...
    'noise_std', 0.00005, ...
    'split_rate', 1/600.0, ...
    'max_hypotheses', 100 ...
    );
[tss, xss, splits] = nslr_base(ts, neutronflux, param);

hold off;
plot(ts, neutronflux);
hold on;
plot(tss, xss);