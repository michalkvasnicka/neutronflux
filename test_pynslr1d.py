import numpy as np
import scipy.signal
import matplotlib.pyplot as plt

import nslr
import faster_nslr
#import nslr_cython as faster_nslr
import cppimport.import_hook
import pynslr1d

# Define noise and split rate of the signal
noise_std = 0.2
split_rate = 1/3.0
np.random.seed(1)

# Create a test signal
ts = np.arange(0, 10.0, 1/60.0)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth((ts)/split_rate, width=0.0) + 10
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std


# Add outliers

outlier_i = np.flatnonzero(np.random.rand(len(xs)) < 0.1)
xs[outlier_i] += np.random.randn(len(outlier_i))*noise_std*5

plt.plot(ts, xs, 'k.', label="Measurments")
plt.plot(ts, xs_truth, 'g-', label='Ground thruth')

# Run tne NSLR
segments = nslr.nslr1d(ts, xs, noise_std, split_rate)
plt.plot(segments.t, segments.x, 'b.-', label="NSLR old")

# Run tne NSLR
segments = faster_nslr.nslr(ts, xs, noise_std, split_rate)
plt.plot(segments.t, segments.x, 'r.-', label="NSLR new", lw=3)

print(segments.t)
splits = pynslr1d.nslr_segments(ts, xs, noise_std, split_rate)
print(splits)
coeffs = pynslr1d.segment_coefficients(ts, xs, splits)
coeffs2 = list(faster_nslr.segment_coefficients(ts, xs.reshape(-1, 1), splits))

for c, c2 in zip(coeffs, coeffs2):
    print(c)
    print(c2)
    print()

ends = pynslr1d.segmented_linear_fit(ts, xs, splits)
plt.plot(ts[splits], ends, 'o-', color='orange', lw=2)
ends = faster_nslr.segmented_linear_fit(ts, xs.reshape(-1, 1), splits)
plt.plot(ts[splits], ends, 'o-', color='orange', lw=2)

plt.show()
