import pyximport; pyximport.install()
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt

#import faster_nslr
import nslr_cython as faster_nslr
import nslr

# Define noise and split rate of the signal
noise_std = 0.2
split_rate = 1/3.0
np.random.seed(0)

# Create a test signal
ts = np.arange(0, 1000, 1/60.0)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth(ts/split_rate, width=0.3) + 10
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std

# Add outliers

outlier_i = np.flatnonzero(np.random.rand(len(xs)) < 0.1)
xs[outlier_i] += np.random.randn(len(outlier_i))*noise_std*5

plt.plot(ts, xs, 'k.', label="Measurments")
plt.plot(ts, xs_truth, 'g-', label='Ground thruth')

# Run tne NSLR
segments = nslr.nslr1d(ts, xs, noise_std, split_rate)
plt.plot(segments.t, segments.x, 'b.-', label="NSLR old")

# Run tne NSLR
segments = faster_nslr.nslr(ts, xs, noise_std, split_rate)
plt.plot(segments.t, segments.x, 'r.-', label="NSLR new", lw=3)



plt.legend()

plt.show()
