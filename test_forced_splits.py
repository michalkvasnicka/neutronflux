import scipy
import matplotlib.pyplot as plt
import numpy as np

from pynslr1d_extra import forced_split_segments, nslr_forced_splits, nslr_forced_splits_auto_std

#data = scipy.io.loadmat('u1c35_210306.mat', simplify_cells=True)
data = scipy.io.loadmat('u2c35_220922.mat', simplify_cells=True)
data = data['data']


rod_change = data['control']

dt = 1.0
temperature = np.array(data['TC'])
neutron_current = np.array(data['SPND'])


#signals = temperature.T
signals = neutron_current.T
ts = np.arange(len(signals[13]))*dt


xs = signals[0]
valid = np.isfinite(xs)
ts = ts[valid]
xs = xs[valid]


noise_std = 0.0005
forced_splits = rod_change
#forced_splits = []
#forced_splits = [10]
#tss, xss, splits = nslr_forced_splits(ts, xs, forced_splits, noise_std, 1/100.0, max_hypotheses=1000)
tss, xss, splits = nslr_forced_splits_auto_std(ts, xs, forced_splits, 1/100.0, max_hypotheses=1000)

for f in forced_splits:
    plt.axvline(ts[f])

plt.plot(ts, xs, 'k-', alpha=0.1)
plt.plot(tss, xss, 'ko-')
plt.show()
