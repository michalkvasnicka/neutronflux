import pyximport; pyximport.install()
import nslr_cython as nslr
import numpy as np
import scipy.signal

wtf = nslr.Hypothesis(0, None, 0, np.nan)
# Define noise and split rate of the signal
noise_std = 0.2
split_rate = 1/3.0
np.random.seed(0)

# Create a test signal
ts = np.arange(0, 10, 1/60.0)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth(ts/split_rate, width=0.3) + 10
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std

print(nslr.inittest(ts, xs, noise_std, split_rate))
#nslr.nslr_segments(ts, xs, noise_std, split_rate)
