#pragma once

#include <cmath>
#include <vector>
#include <list>
#include <algorithm>
#include <memory>
#include <queue>
#include <cmath>
#include <valarray>

#include <iostream>
#include <cassert>

#include "normal.h"

double NaN = std::nan("");
using std::log;
using std::sqrt;
using std::erf;
using std::pow;
const double pi = 3.14159265358979323846;
typedef unsigned int uint;

struct NslrHypothesis {
	
	uint j;
	std::shared_ptr<NslrHypothesis> parent;
	double lik = 0.0;
	double total_lik = 0.0;

	double a = NaN;
	double b = 0.0;

	uint n = 0;
	double t = 0.0;
	double St = 0.0;
	double Stt = 0.0;
	double S = 0.0;

	double Sx = 0.0;
	double Sxx = 0.0;
	double Stx = 0.0;

	NslrHypothesis(uint j, std::shared_ptr<NslrHypothesis> parent, double lik, double b)
		:j(j), parent(parent), lik(lik), total_lik(lik), b(b) 
	{
		//std::cout << j << " Got created" << std::endl;
	};

	static const bool compare_lik(const NslrHypothesis& a, const NslrHypothesis b) {
		return a.lik < b.lik;
	}
	
	NslrHypothesis() = delete;
	NslrHypothesis& operator=(NslrHypothesis const&) = delete;
	NslrHypothesis(const NslrHypothesis& other) = delete;
	/*~NslrHypothesis() {
		std::cout << j << " Got deallocated!!" << std::endl;
	}*/
};

typedef std::shared_ptr<NslrHypothesis> NslrHypothesisPtr;
typedef std::vector<NslrHypothesisPtr> NslrHypotheses;
typedef std::valarray<uint> Uints;
typedef std::valarray<double> Doubles;

struct Nslr {
	NslrHypotheses hypotheses;
	uint i;
	Nslr() {
		auto root = std::make_shared<NslrHypothesis>(0, nullptr, 0.0, NaN);
		hypotheses.push_back(root);
		i = 0;
	}

	void step(double dt, double x, double noise_std, double split_rate, uint max_hypotheses=0) {
		auto winner_ptr = *hypotheses.begin();
		auto loser_ptr = *hypotheses.begin();
		auto loser_idx = 0;
		for(uint h_i = 0; h_i < hypotheses.size(); ++h_i) {
			auto hp = hypotheses[h_i];
			assert(hp);
            		auto& h = *hp;
			h.n += 1; h.t += dt; h.St += h.t; h.Stt += h.t*h.t;
            		h.Sx += x; h.Sxx += x*x; h.Stx += h.t*x;
			if(!h.parent) {
                		if(h.St*h.St - h.n*h.Stt != 0) {
                    			h.b = (h.St*h.Stx - h.Stt*h.Sx)/(h.St*h.St - h.n*h.Stt);
				} else {
                    			h.b = h.Sx/h.n;
				}
			}

			auto S = 0.0;
			if(h.Stt > 0.0) {
				h.a = (h.Stx - h.b*h.St)/h.Stt;
                		S = h.a*h.a*h.Stt + 2*h.a*h.b*h.St - 2*h.a*h.Stx + h.n*h.b*h.b - 2*h.b*h.Sx + h.Sxx;
			}

            		h.lik += log(1/(sqrt(2*pi)*noise_std)) + (h.S - S)/(2*noise_std*noise_std);
			assert(std::isfinite(h.lik));
			h.S = S;
			
			auto mean = 1.0/(split_rate);
            		auto sigma = 1.0/sqrt(6.0);
            		auto mu = log(mean) - (sigma*sigma/2.0);
			auto z = (log(h.t) - mu)/sigma;
            		//auto dur_lik = log(
			//		0.5 + 0.5*erf(z/sqrt(2.0))
			//		);
			//auto dur_lik = log(1 / (1 + 2*exp(-sqrt(2*pi)*x)));
			// TODO: Vast majority of time is spent here.
			auto dur_lik = logcdf(z);
			//auto dur_lik = log(1 - exp(-split_rate*(h.t)));
			h.total_lik = h.lik + dur_lik;

			if(h.total_lik > winner_ptr->total_lik) {
				winner_ptr = hp;
			}

			if(h.total_lik < loser_ptr->total_lik) {
				loser_ptr = hp;
				loser_idx = h_i;
			}
		}
		
		auto& winner = *winner_ptr;
		auto split_lik = log(1.0-exp(-split_rate*dt));
		auto new_lik = winner.total_lik; //+ split_lik;
		if(i == 0 || std::isinf(new_lik)) {
			++i;
			return;
		}
		
		
		auto winner_end = winner.t*winner.a + winner.b;
		auto new_h = std::make_shared<NslrHypothesis>(i, winner_ptr, new_lik, winner_end);
		
		// TODO: This does not ensure that the removed hypothesis couldn't
		// be the winner! Probably due to non-additivity of the dur_lik.
		// Hacking some hardcoded buffer for now.
		if(max_hypotheses == 0) {
			auto pruning_slack = 0.1;
			auto dead = std::remove_if(
				hypotheses.begin(), hypotheses.end(),
				[new_lik, pruning_slack](auto &h) { return h->lik < new_lik - pruning_slack; }
				);
			hypotheses.erase(dead, hypotheses.end());
			hypotheses.emplace_back(new_h);
		} else if(max_hypotheses > 0 && hypotheses.size() >= max_hypotheses) {
			/*std::sort(hypotheses.begin(), hypotheses.end(),
					[](auto &a, auto &b) {
						return a->lik > b->lik;	
					}
				 );
			hypotheses.resize(max_hypotheses);
			hypotheses.emplace_back(new_h);*/
			hypotheses[loser_idx] = new_h;
		} else {
			hypotheses.emplace_back(new_h);
		}

		++i;

	}

	Uints splits() {
		std::vector<uint> splits;
			
		// TODO: Should use total_lik!?!
		auto h = *std::max_element(
				hypotheses.begin(), hypotheses.end(),
				[](auto& a, auto& b) { return a->total_lik < b->total_lik; });
		while(h) {
			splits.push_back(h->j);
			h = h->parent;
		}
		std::reverse(splits.begin(), splits.end());
		if(splits.back() < i - 1) {
			splits.push_back(i - 1);
		}
		Uints splits_arr(splits.data(), splits.size());
		return splits_arr;
	}
};


Uints nslr_segments(Doubles ts, Doubles xs, double noise_std, double split_rate, double max_hypotheses=0) {
	Nslr nslr;

	double prev_t = ts[0];
	uint length = std::min(ts.size(), xs.size());
	
	for(uint i=0; i < length; ++i) {
		double t = ts[i];
		double x = xs[i];
		
		double dt = t - prev_t; prev_t = t;
		nslr.step(dt, x, noise_std, split_rate, max_hypotheses);
	}
	
	return nslr.splits();
}

// TODO: Not a very performant implementation, but make it work right!
auto segment_coefficients(Doubles ts, Doubles xs, Uints J) {
	std::vector<std::tuple<double, double, double, double>> coeffs;
	auto Sxm0 = 0.0; auto Smw0 = 0.0; auto Sxw0 = 0.0; auto Sww0 = 0.0; auto Smm0 = 0.0;
	for(uint k=0; k < J.size() - 1; ++k) {
		auto n = J[k+1] - J[k];
		auto span = std::slice(J[k], n, 1);
		//auto span = std::slice(J[k], n, 1);
		auto t = std::valarray(ts[span]);
		auto x = std::valarray(xs[span]);

		auto dur = (t[n-1] - t[0]);
		if(dur == 0) {
			dur = 1.0;
		}

		auto w = (t - t[0])/dur;
		auto m = 1 - w;
		
		auto Smw1 = (m*w).sum();
		auto Smm1 = (m*m).sum();
		auto Sxm1 = (x*m).sum();

		auto p0 = Smw0;
		auto p1 = Smm1 + Sww0;
		auto p2 = Smw1;
		auto y = Sxm1 + Sxw0;

		coeffs.emplace_back(p0, p1, p2, y);
		
		Smw0 = Smw1;
		Smm0 = Smm1;
		Sxm0 = Sxm1;
		Sww0 = (w*w).sum();
		Sxw0 = (x*w).sum();
	}
	
	auto p0 = Smw0;
	auto p1 = 0.0 + Sww0;
	auto p2 = 0.0;
	auto y = 0.0 + Sxw0;
	coeffs.emplace_back(p0, p1, p2, y);

	return coeffs;

};

// TODO: Not a very performant implementation, but make it work right!
auto segmented_linear_fit(Doubles ts, Doubles xs, Uints J) {
	auto coeffs = segment_coefficients(ts, xs, J);
	std::vector<std::pair<double, double>> bgs;
	bgs.emplace_back(0.0, 0.0);
	for(auto c : coeffs) {
		double p0, p1, p2, y, b, g;
		std::tie(p0, p1, p2, y) = c;
		std::tie(b, g) = bgs.back();
        	auto denom = p0*g + p1;
        	bgs.emplace_back((y - p0*b)/denom, -p2/denom);
	}

	auto endpoint = 0.0;
	std::vector<double> endpoints;

	for(int i=bgs.size()-1; i >= 1; --i) {
		double b, g;
		std::tie(b, g) = bgs[i];
		endpoint = g*endpoint + b;
		endpoints.push_back(endpoint);
	}

	std::reverse(endpoints.begin(), endpoints.end());
	return endpoints;
}

auto nslr(Doubles ts, Doubles xs, double noise_std, double split_rate, double max_hypotheses=0) {
	auto splits = nslr_segments(ts, xs, noise_std, split_rate, max_hypotheses);
	// TODO: Hack the last split
	std::vector<double> tss(splits.size());
	for(auto i=0; i < splits.size(); ++i) {
		tss[i] = ts[splits[i]];
	}
	
	auto xss = segmented_linear_fit(ts, xs, splits);
	
	return std::make_tuple(tss, xss, splits);
}

