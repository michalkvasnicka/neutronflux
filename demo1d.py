import nslr
import faster_nslr as slow_nslr
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal

# Define noise and split rate of the signal
noise_std = 1.0
split_rate = 1/3.0

# Create a test signal
ts = np.arange(0, 10, 1/60.0)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth(ts/split_rate, width=0.5) + 10
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std

# Run tne NSLR
segments = nslr.nslr1d(ts, xs, noise_std, split_rate)

# Run the slow NSLR
segments_slow = slow_nslr.nslr(ts, xs.reshape(-1, 1), np.array([noise_std]), split_rate*2.0)
segments_slow.x = np.array(segments_slow.x).reshape(-1)

# Plot the results
plt.plot(ts, xs, 'k.', label="Measurments")
plt.plot(ts, xs_truth, 'g-', label='Ground thruth')
plt.plot(segments.t, segments.x, 'r.-', label="NSLR result")
plt.plot(segments_slow.t, segments_slow.x, 'b.-', label="Slow NSLR result")
plt.xlabel("Time")
plt.ylabel("Signal value")
plt.legend()
plt.show()

